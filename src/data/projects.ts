import type { Project } from '../types/Project';

const projects: Project[] = [
	{
		id: 'jupiterui',
		name: 'JupiterUI',
		description: 'The elegant and reliable UI kit for web artisans.',
		repository: 'https://codeberg.org/JupiterUI/JupiterUI',
		language: 'SCSS'
	},
	{
		id: 'infinium-site',
		name: 'Infinium Website',
		description: "Our website (the one you're viewing right now)",
		repository: 'https://github.com/Infinium8/website',
		language: 'TypeScript'
	},
];

export {
	projects
}