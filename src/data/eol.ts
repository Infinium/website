interface TEOL {
	text: string,
	href: string,
	link: string
}

const eol: Array<TEOL> = [
	{
		text: 'Questions? Comments?',
		href: '/contact',
		link: 'Email us.'
	},
	{
		text: 'Feeling magnanimous?',
		href: '/donate',
		link: 'Buy us a coffee.'
	},
	{
		text: 'Want to support open-source?',
		href: '/donate',
		link: 'Make a small donation.'
	},
	{
		text: 'This website is',
		href: 'https://codeberg.org/athena/athena',
		link: 'open-source.'
	},
];

export {
	eol
}

export type { TEOL }