import type { NextPage } from 'next';
import Link from 'next/link';

import SplitSection from '../components/layout/SplitSection';
import PageTitle from '../components/PageTitle';

import type { ChildrenOnly } from '../types/utilities';

interface DonateLinkProps {
	href: string;
	children: React.ReactNode;
}

const DonateLink = ({ href, children }: DonateLinkProps) => (
	<Link href={href} passHref>
		<a
			rel='noreferrer noopener nofollow'
			target='_blank'
			className='button mt-1r mr-1r d-inline-flex'
		>
			{children}
		</a>
	</Link>
);

const Donate: NextPage = () => {
	return (
		<>
		<PageTitle>Donate</PageTitle>
		<SplitSection
			caption='Make a donation'
			title='Buy us a coffee'
			content={
				<>
					If you support our cause, enjoy our work or find it useful,
					making a donation is a great (and very helpful) way to show
					your appreciation. It allows us to continue to maintain
					free, open-source projects that you or the products you love
					make use of.
				</>
			}
			linkArea={
				<div className='flex flex-row'>
					<DonateLink href='https://venmo.com/u/fiveatoms'>
						Venmo
					</DonateLink>
					<DonateLink href='https://liberapay.com/athena/donate'>
						Liberapay
					</DonateLink>
				</div>
			}
			image={{
				src: '/images/pages/donate/coffee-spilling.png',
				alt: '3D coffee cup with coffee spilling out',
				width: 500,
				height: 450,
			}}
		/>
		</>
	);
};

export default Donate;
