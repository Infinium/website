import type { AppProps } from 'next/app';
import { ThemeProvider } from 'next-themes';

import Layout from '../components/layout/Layout';

import '../assets/css/global.scss';
import '../assets/media/fonts/inter.css';

const App = ({ Component, pageProps }: AppProps) => {
	return (
		<ThemeProvider attribute="class">
			<Layout>
				<Component {...pageProps} />	
			</Layout>
		</ThemeProvider>
	);
}

export default App;