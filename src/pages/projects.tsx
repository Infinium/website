import type { NextPage } from 'next';

import SplitSection from '../components/layout/SplitSection';
import View from '../components/layout/View';
import PageTitle from '../components/PageTitle';
import ProjectCard from '../components/ProjectCard';

import { projects } from '../data/projects';

const Projects: NextPage = () => {
	return (
		<>
			<PageTitle>Projects</PageTitle>

			<SplitSection
				caption='Open-source'
				title='Our Projects'
				content={
					<>
						We've listed some of our current open-source projects.
						We'll be adding more as the days go by.
					</>
				}
				link={{
					label: 'Find us on GitHub',
					href: 'https://github.com/Infinium8',
					newTab: true,
				}}
				image={{
					src: '/images/pages/projects/planet.png',
					alt: '3D globe',
					width: 500,
					height: 290,
				}}
			/>

			<View mods={{ section: 'pt-0' }}>
				<div className='w-100p grid grid-3 landscape-grid-1'>
					{projects.map(e => (
						<div className='grid-block' key={e.id}>
							<ProjectCard {...e} />
						</div>
					))}
				</div>
			</View>
		</>
	);
};

export default Projects;
