import type { NextPage } from 'next';

import SplitSection from '../components/layout/SplitSection';
import PageTitle from '../components/PageTitle';

const Contact: NextPage = () => {
	return (
		<>
			<PageTitle>Contact us</PageTitle>

			<SplitSection
				caption='Get in touch'
				title='Contact us'
				content={
					<>
						<span>
							If you have any questions about what we do, how we
							do it, or how we can help, feel free to send us an
							email. We're also available for enterprise
							consultation on a per-project basis.
						</span>

						<p className='mb-0 mt-1r text-dynamic-06 fs-lg'>
							infinium-llc@protonmail.com
						</p>
					</>
				}
				link={{
					label: 'Send us an email',
					href: 'mailto:infinium-llc@protonmail.com',
				}}
				image={{
					src: '/images/pages/contact/globe.png',
					alt: '3D globe',
					width: 500,
					height: 550,
				}}
			/>
		</>
	);
};

export default Contact;
