import ArticleCard from '../components/ArticleCard';
import View from '../components/layout/View';
import PageTitle from '../components/PageTitle';
import Title from '../components/typography/Title';

import { getAllArticles } from '../lib/api';
import type { Article } from '../lib/types';

interface Props {
	articles: Array<Article>;
}

const Articles = ({ articles }: Props) => {
	return (
		<>
			<PageTitle>Articles</PageTitle>

			<View mods={{ section: 'pt-8r' }}>
				<Title>Articles</Title>

				<div className='w-100p grid grid-4 wide-grid-3 tablet-grid-2 landscape-grid-1 mt-3r gap-1-5r'>
					{articles.map((e: Article) => (
						<ArticleCard key={e.title} {...e} />
					))}
				</div>
			</View>
		</>
	);
};

export default Articles;

export const getStaticProps = async () => {
	const articles = getAllArticles([
		'title',
		'date',
		'slug',
		'image',
		'image_description',
		'description',
		'content',
		'author',
		'type',
	]);

	return {
		props: { articles },
	};
};
