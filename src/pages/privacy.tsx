import type { NextPage } from 'next';

import SplitSection from '../components/layout/SplitSection';
import PageTitle from '../components/PageTitle';

const Privacy: NextPage = () => {
	return (
		<>
			<PageTitle>Privacy</PageTitle>

			<SplitSection
				caption='Privacy'
				title="You're anonymous"
				content={
					<>
						Don't worry, we'll keep this short. This website does
						not collect any data whatsoever. We make a living from
						the generous donations of people and businesses around
						the world, not by selling your data to sketchy brokers.
						Isn't that refreshing?
					</>
				}
				link={{
					label: 'Questions? Contact us.',
					href: '/contact',
				}}
				image={{
					src: '/images/pages/privacy/ghost.png',
					alt: '3D green-colored ghost smiling',
					width: 500,
					height: 520,
				}}
			/>
		</>
	);
};

export default Privacy;
