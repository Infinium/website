import type { NextPage } from 'next';
import { useLottie } from 'lottie-react';

import View from '../components/layout/View';
import Link from '../components/Link';
import PageTitle from '../components/PageTitle';

import infinityAnimation from '../../public/extras/infinityAnimation.json';

const Infinity = () => {
	const options = {
		animationData: infinityAnimation,
		loop: true,
		autoplay: true,
	};

	const { View } = useLottie(options);

	return View;
};

const FourOFour: NextPage = () => {
	return (
		<>
			<PageTitle>Page Not Found - Infinium</PageTitle>

			<View mods={{ section: 'pt-4r', container: 'flex-c' }}>
				<div className='w-100p flex-c'>
					<div className='dark-invert'>
						<Infinity />
					</div>

					<div className='flex-c text-c mt-4r mw-30r'>
						<h1 className='fs-8xl fw-700 mb-2r'>404</h1>
						<p className='fs-xl fw-500 lh-1-7'>
							The page you were looking for was not found. Hurry
							and head back home, otherwise you'd be stuck here
							forever.
						</p>
						<Link href='/' mods='underline'>
							Head back home
						</Link>
					</div>
				</div>
			</View>
		</>
	);
};

export default FourOFour;
