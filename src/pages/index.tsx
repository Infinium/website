import type { NextPage } from 'next';
import Head from 'next/head';
import Image from 'next/image';

import View from '../components/layout/View';
import Title from '../components/typography/Title';
import Link from '../components/Link';
import Caption from '../components/typography/Caption';
import SplitSection from '../components/layout/SplitSection';
import PageTitle from '../components/PageTitle';

interface SectionProps {
	caption: string;
	title: string;
	content: React.ReactNode;
	link: string;
	href: string;
}

const ContentArea = ({ caption, title, content, link, href }: SectionProps) => {
	return (
		<>
			<Caption>{caption}</Caption>
			<Title>{title}</Title>

			<div className='mw-44r flex'>
				<p className='fs-2xl lh-1-7'>{content}</p>
				<Link href={href} mods='button' isJLink={false}>
					{link}
				</Link>
			</div>
		</>
	);
};

const Home: NextPage = () => {
	return (
		<>
			<PageTitle>Home</PageTitle>

			<View mods={{ section: 'border-bottom-ui-2' }}>
				<div className='w-100p grid grid-2 landscape-grid-1'>
					<div className='grid-block'>
						<p className='fs-lg text-dynamic-06'>
							Nice to meet you
						</p>
						<h1 className='lh-1-2'>
							We build and maintain open-source software.
						</h1>

						<div className='mw-35r flex'>
							<p className='fs-2xl lh-1-7 text-dynamic-07'>
								We're a small team of software engineers
								dedicated to building and maintaining a variety
								of useful open-source software projects.
							</p>
							<a href='#what-we-do' className='button'>
								What we do
							</a>
						</div>
					</div>
					<div className='grid-block align-e'>
						<Image
							src='/images/pages/home/hero.png'
							alt='3D scene of girl sitting down holding a tablet next to a pile of books and two plants.'
							width={500}
							height={400}
						/>
					</div>
				</div>
			</View>

			<View id='what-we-do' mods={{ section: 'pb-0' }}>
				<ContentArea
					caption='What we do'
					title='Building open-source'
					content={
						<>
							The digital world is built upon open-source
							software. Millions of projects use open-source
							software in one way or another. We're a small team
							of software engineers dedicated to the production
							and maintenance of useful open-source projects.
						</>
					}
					link='Our Projects'
					href='/projects'
				/>
			</View>

			<div className='w-100p flex align-e'>
				<img src='/images/pages/home/workspace.png' className='w-100p' />
			</div>

			<SplitSection
				caption='What we do'
				title='Enterprise collaboration'
				content={
					<>
						If you're an enterprise making use of our software,
						please consider <Link href='/donate'>donating. </Link>
						It helps us continue to maintain the project your
						business depends on. If you need something custom, we'll
						work with you and your team to create highly-tailored
						open-source projects for your business.
					</>
				}
				link={{
					href: '/contact',
					label: 'Get in touch',
				}}
				image={{
					src: '/images/pages/home/collab.png',
					alt: '3D man sitting on the ground looking at phone',
					width: 450,
					height: 480,
				}}
			/>
		</>
	);
};

export default Home;
