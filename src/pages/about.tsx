import type { NextPage } from 'next';

import SplitSection from '../components/layout/SplitSection';
import PageTitle from '../components/PageTitle';

const About: NextPage = () => {
	return (
		<>
			<PageTitle>What we do</PageTitle>

			<SplitSection
				viewMods={{ section: 'border-bottom-ui-2' }}
				caption='Our work'
				title='Open-source for everyone'
				content={
					<>
						We build and maintain a variety of useful open-source
						projects. The categories we work in range all the way
						from local automation scripts to more globalized UI
						kits.
					</>
				}
				link={{
					label: 'View Our Projects',
					href: '/projects',
				}}
				image={{
					src: '/images/pages/about/laptop-with-books.png',
					alt: '3D Laptop with two planetary illustrations next to a stack of books and cup of coffee',
					width: 500,
					height: 500,
				}}
			/>

			<SplitSection
				viewMods={{ section: 'border-bottom-ui-2' }}
				caption='Our work'
				title='Relying on donations'
				content={
					<>
						Maintaining open-source projects is an exhaustive task.
						It takes an incredible amount of time and effort to keep
						these projects enterprise-ready. We rely on the generous
						donations of people and businesses just like you. If you
						make use of some of our projects, please consider
						donating.
					</>
				}
				link={{
					label: 'Make a donation',
					href: '/donate',
				}}
				image={{
					src: '/images/pages/about/molecule.png',
					alt: '3D Laptop with two planetary illustrations next to a stack of books and cup of coffee',
					width: 400,
					height: 700,
				}}
			/>
		</>
	);
};

export default About;
