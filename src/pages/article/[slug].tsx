import { useEffect } from 'react';
import { useRouter } from 'next/router';
import { motion } from 'framer-motion';
import classNames from 'classnames';
import ErrorPage from 'next/error';

import type { Article } from '../../lib/types';

import View from '../../components/layout/View';
import EOL from '../../components/EOL';
import PageTitle from '../../components/PageTitle';

import { getArticleBySlug, getAllArticles } from '../../lib/api';
import { colorMap } from '../../lib/colors';
import markdownToHtml from '../../lib/markdownToHtml';

interface Props {
	article: Article;
	morePosts: Array<any>;
	preview: any;
}

const SingleArticle = ({ article, morePosts, preview }: Props) => {
	const router = useRouter();

	useEffect(() => {
		const bdc = document.body.classList;
		const color = getColor().slice(0, -3);

		bdc.add(color);

		return () => bdc.remove(color);
	}, []);
	
	if (!router.isFallback && !article?.slug) {
		return <ErrorPage statusCode={404} />;
	}

	const getColor = () => colorMap[article.type];

	const variants = {
		initial: { opacity: 0, scale: 0.96, y: 80 },
		animate: { opacity: 1, scale: 1, y: 0 },
	};

	return (
		<>
			<View mods={{ section: 'pt-14r tablet-pt-10r' }}>
				{router.isFallback ? (
					<p>Loading...</p>
				) : (
					<>
						<PageTitle>{article.title}</PageTitle>

						<div className='w-100p flex-c'>
							<motion.div
								variants={variants}
								initial='initial'
								animate='animate'
								transition={{
									type: 'spring',
									duration: 0.8,
									delay: 0.1,
								}}
								className='mw-40r mb-2r'
							>
								<div
									className={classNames(
										'd-inline-flex mb-1r ph-0-75r py-0-5r radius-90',
										`bg-${getColor()}`
									)}
								>
									<span className='text-upper fw-600 fs-sm ls-md text-white'>
										{article.type}
									</span>
								</div>
								<p className='mt-0-5r fs-lg fw-500 text-dynamic-08'>
									{article.date}
								</p>
								<h1 className='fs-5xl tablet-fs-4xl fw-600 mb-1-5r'>
									{article.title}
								</h1>
								<p className='fs-2xl landscape-fs-xl fw-400 mb-1-5r text-dynamic-08'>
									{article.description}
								</p>
								<p className="fs-lg text-dynamic-07">
									Author: {article.author}
								</p>
							</motion.div>

							<motion.div
								variants={variants}
								initial='initial'
								animate='animate'
								transition={{
									type: 'spring',
									duration: 0.8,
									delay: 0.3,
								}}
								className='flex-c mw-60r w-100p text-c mb-3r'
							>
								<img
									src={article.image}
									className='radius-4 w-100p'
								/>
								<p className='fs-sm mt-1r'>
									{article.image_description}
								</p>
							</motion.div>

							<article
								className={classNames(
									'mw-40r w-100p',
									getColor()
								)}
								content-size='lg'
								dangerouslySetInnerHTML={{
									__html: article.content,
								}}
							></article>
						</div>
					</>
				)}
			</View>

			<EOL color={getColor()} />
		</>
	);
};

export default SingleArticle;

export async function getStaticProps({ params }: { params: any }) {
	const article = getArticleBySlug(params.slug, [
		'title',
		'date',
		'slug',
		'author',
		'content',
		'description',
		'image',
		'image_description',
		'type',
	]);
	const content = await markdownToHtml(article.content || '');

	return {
		props: {
			article: {
				...article,
				content,
			},
		},
	};
}

export async function getStaticPaths() {
	const articles = getAllArticles(['slug']);

	return {
		paths: articles.map((article: Article) => {
			return {
				params: {
					slug: article.slug,
				},
			};
		}),
		fallback: false,
	};
}
