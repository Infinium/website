import Head from 'next/head';

interface Props {
	children?: React.ReactNode,
	custom?: string
}

const PageTitle = ({ children, custom }: Props) => (
	<Head>
		<title>{children} - Infinium</title>
	</Head>
);

export default PageTitle;
