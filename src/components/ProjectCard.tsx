import Link from 'next/link';
import classNames from 'classnames';

import type { Project } from '../types/Project';

const ProjectCard = ({
	id,
	name,
	description,
	repository,
	language,
}: Project) => {
	const colorMap: any = {
		SCSS: 'magenta',
		TypeScript: 'cyan'
	};

	return (
		<Link href={repository} passHref>
			<a
				id={id}
				className='j-card clickable w-100p'
				rel='noreferrer noopener nofollow'
				target='_blank'
			>
				<div className='j-card-content flex-2'>
					<div>
						<h4>{name}</h4>
						<p>{description}</p>
					</div>
					<div className="mt-1r flex">
						<div
							className={classNames(
								'ph-0-5r py-0-25r radius-4',
								`bg-${colorMap[language]}-10`
							)}
						>
							<span
								className={classNames(
									'fw-500 fs-sm',
									`text-${colorMap[language]}-60`
								)}
							>
								{language}
							</span>
						</div>
					</div>
				</div>
			</a>
		</Link>
	);
};

export default ProjectCard;