import MenuLink from './MenuLink';

import { links } from './data';

const Menu = () => {
	return (
		<div className='menu-links h-100p flex flex-row align-c justify-c'>
			{links.map(e => (
				<MenuLink key={e.id} href={e.href}>
					{e.label}
				</MenuLink>
			))}
		</div>
	);
};

export default Menu;
