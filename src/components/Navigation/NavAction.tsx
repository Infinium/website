interface Props {
	onClick?(): any,
	icon?: JSX.Element
}

const NavAction = ({ icon, onClick }: Props) => {
	return (
		<button className="nav-action" onClick={onClick}>
			<i className="j-icon">{icon}</i>
		</button>
	);
};

export default NavAction;