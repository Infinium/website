interface MenuLink {
	id: string,
	label: string,
	href: string
}

const links: Array<MenuLink> = [
	// {
	// 	id: 'home',
	// 	label: 'Home',
	// 	href: '/'
	// },
	{
		id: 'what-we-do',
		label: 'What we do',
		href: '/about'
	},
	{
		id: 'projects',
		label: 'Projects',
		href: '/projects'
	},
	{
		id: 'articles',
		label: 'Articles',
		href: '/articles'
	},
	{
		id: 'donate',
		label: 'Donate',
		href: '/donate'
	}
];

export {
	links
}