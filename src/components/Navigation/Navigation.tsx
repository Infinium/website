import { useEffect, useState } from 'react';

import Link from '../Link';
import Container from '../layout/Container';
import Menu from './Menu';
import NavMenu from './NavMenu';
import ThemeButton from './ThemeButton';
import Logo from '../Logo';

const Navigation = () => {
	const [navClass, setNavClass] = useState<string | undefined>(undefined);

	const scroll = () => {		
		if (document.body.scrollTop > 0 || document.documentElement.scrollTop > 0) {			
			setNavClass('scrolled');
		} else {
			setNavClass(undefined);
		}
	}

	useEffect(() => {
		window.onscroll = () => scroll();
	}, []);

	return (
		<nav className={navClass}>
			<Container mods='h-100p flex align-c justify-sb flex-row'>
				<div className='w-100p h-100p grid grid-3 landscape-grid-2'>
					<div className='grid-block h-100p'>
						<Logo />
					</div>

					<div className='grid-block h-100p align-c landscape-hide'>
						<Menu />
					</div>

					<div className='grid-block h-100p flex flex-row align-c justify-e landscape-hide'>
					<ThemeButton />
						<Link href='/contact' mods='nav-button ml-1r' isJLink={false}>
							Contact
						</Link>
					</div>

					<div className='grid-block h-100p align-e hide landscape-nohide'>
						<NavMenu />
					</div>
				</div>
			</Container>
		</nav>
	);
};

export default Navigation;
