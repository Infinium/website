import { useRouter } from 'next/router';
import Link from 'next/link';
import classNames from 'classnames';

interface Props {
	children: any;
	href: string;
}

const MenuLink = ({ href, children }: Props) => {
	const { pathname } = useRouter();

	return (
		<Link href={href} passHref>
			<a className={classNames('menu-link', pathname === href ? 'active' : '')}>
				<span>{children}</span>
			</a>
		</Link>
	);
};

export default MenuLink;
