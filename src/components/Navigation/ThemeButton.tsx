import { AnimatePresence, motion } from 'framer-motion';
import { useTheme } from 'next-themes';

import { icons } from '../../data/icons';

const Icon = ({ children }: { children: any }) => {
	const variants = {
		initial: { x: -30, y: 30, rotate: -90 },
		animate: { x: 0, y: 0, rotate: 0 },
		exit: { x: 30, y: 30, rotate: 90 },
	};

	return (
		<motion.i
			variants={variants}
			initial='initial'
			animate='animate'
			exit='exit'
			transition={{
				type: 'spring',
				duration: 0.8,
				bounce: 0.4,
			}}
			className='j-icon'
		>
			{children}
		</motion.i>
	);
};

const ThemeButton = () => {
	const { theme, setTheme } = useTheme();

	return (
		<button
			className='nav-action tablet-hide'
			onClick={() => setTheme(theme === 'dark' ? 'light' : 'dark')}
		>
			<AnimatePresence>
				{theme === 'dark' ? (
					<Icon key='dark'>{icons.sun}</Icon>
				) : (
					<Icon key='light'>{icons.moon}</Icon>
				)}
			</AnimatePresence>
		</button>
	);
};

export default ThemeButton;
