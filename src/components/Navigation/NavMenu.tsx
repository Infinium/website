import { useState } from 'react';
import { motion } from 'framer-motion';
import { useTheme } from 'next-themes';
import Link from 'next/link';
import classNames from 'classnames';
import { links } from './data';

const NavMenu = () => {
	const [menu, setMenu] = useState<boolean>(false);
	const { theme, setTheme } = useTheme();

	const containerVariants = {
		tap: {},
		hover: {},
	};
	const circleVariants = {
		hover: { scale: 1.2 },
		tap: { scale: 0.9 },
	};

	return (
		<div>
			<motion.div
				variants={containerVariants}
				whileHover='hover'
				whileTap='tap'
				className='j-dropdown relative-right'
				aria-expanded={menu}
			>
				<button
					className='j-dropdown-trigger p-0 m-0 bg-transparent cursor-pointer'
					onClick={() => setMenu(!menu)}
				>
					<div className='flex flex-row align-c'>
						<span className='fw-500 text-dynamic'>Menu</span>
						<div className='relative h-1-75r w-1-75r ml-0-75r flex-c'>
							<motion.div
								variants={circleVariants}
								transition={{
									type: 'spring',
									duration: 0.8,
									bounce: 0.4
								}}
								className='menu-circle ui dark-bg-gray-90 radius-90 h-100p w-100p absolute top-0 right-0 bottom-0 left-0'
							></motion.div>
							<div
								className={classNames(
									'menu-icon',
									menu && 'active'
								)}
							>
								<div></div>
								<div></div>
							</div>
						</div>
					</div>
				</button>
				<ul
					className='j-dropdown-menu slide-down shadow-md'
					onMouseLeave={() => setMenu(false)}
				>
					<li>
						{links.map(e => (
							<Link key={e.id} href={e.href} passHref>
								<a className='j-dropdown-link'>{e.label}</a>
							</Link>
						))}
					</li>
					<li className='j-dropdown-divider'></li>
					<li>
						<a
							className='j-dropdown-link'
							onClick={() =>
								setTheme(theme === 'dark' ? 'light' : 'dark')
							}
						>
							{theme === 'dark' ? 'Light mode' : 'Dark mode'}
						</a>
					</li>
				</ul>
			</motion.div>
		</div>
	);
};

export default NavMenu;
