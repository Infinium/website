import { icons } from "../../data/icons";

const CopyrightArea = () => (
	<div className='w-100p flex-c border-top-ui-2 py-1-5r'>
		<div className='container'>
			<div className='w-100p flex-sb landscape-flex-column'>
				<span className='text-dynamic-07 fs-sm'>
					Copyright &copy; 2021 Infinium LLC. All rights reserved.
				</span>
				<p className="mb-0 d-inline-flex align-c landscape-mt-1r">
					Made with{' '}
					<i className="j-icon sm text-red-50 mh-0-25r">{icons.heart}</i>
					{' '}in Salt Lake City.
				</p>
			</div>
		</div>
	</div>
);

export default CopyrightArea;
