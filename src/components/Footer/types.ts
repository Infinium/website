interface LinkProps {
	id: string,
	label: string,
	href: string,
	newTab?: boolean
}

interface Column {
	title: string,
	links: LinkProps[]
}

export type { LinkProps, Column }