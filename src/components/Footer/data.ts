import type { Column } from "./types";

const columns: Column[] = [
	{
		title: 'Infinium',
		links: [
			{
				id: 'about-us',
				label: 'About us',
				href: '/about'
			},
			{
				id: 'articles',
				label: 'Articles',
				href: '/articles'
			},
		]
	},
	{
		title: 'Our Work',
		links: [
			{
				id: 'projects',
				label: 'Projects',
				href: '/projects'
			},
			{
				id: 'donate',
				label: 'Donate',
				href: '/donate'
			}
		]
	},
	{
		title: 'Other',
		links: [
			{
				id: 'github',
				label: 'GitHub',
				href: 'https://github.com/Infinium8',
				newTab: true
			},
			// {
			// 	id: 'about-us',
			// 	label: 'About us',
			// 	href: '/'
			// },
			{
				id: 'privacy',
				label: 'Privacy',
				href: '/privacy'
			},
		]
	}
];

export {
	columns
}