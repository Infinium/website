import Columns from "./Columns";
import CopyrightArea from "./CopyrightArea";

const Footer = () => {
	return (
		<section className="w-100p border-top-ui-2">
			<Columns />
			<CopyrightArea />
		</section>
	);
};

export default Footer;