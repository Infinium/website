import Link from '../Link';
import Logo from '../Logo';

const InfoColumn = () => (
	<div className='grid-block justify-s landscape-align-c landscape-text-c'>
		<Logo />

		<div className='mw-14r'>
			<p className='mt-1r'>
				Building and maintaining the future of open-source.
			</p>
			<Link href='/contact' mods='underline no-bg d-inline'>
				Contact us
			</Link>
		</div>
	</div>
);

export default InfoColumn;
