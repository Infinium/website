import Column from './Column';
import InfoColumn from './InfoColumn';

import { columns } from './data';

const Columns = () => {
	return (
		<div className='container py-4r'>
			<div className='w-100p grid grid-4 gap-3r tablet-grid-2 landscape-grid-1'>
				<InfoColumn />

				{columns.map(e => (
					<Column key={e.title} {...e} />
				))}
			</div>
		</div>
	);
};

export default Columns;
