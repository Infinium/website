import Link from '../Link';
import type { LinkProps } from './types';

interface Props {
	title: string;
	links: LinkProps[];
}

const Column = ({ title, links }: Props) => {
	const FooterLink = ({ id, href, label, newTab = false }: LinkProps) => (
		<Link
			href={href}
			isJLink={false}
			mods='j-link underline no-bg mb-1r'
			newTab={newTab}
		>
			<span>{label}</span>
		</Link>
	);

	return (
		<div className='grid-block landscape-align-c landscape-text-c justify-s'>
			<h4 className='fs-base text-dynamic-07 fw-400'>{title}</h4>

			{links.map(e => (
				<FooterLink key={e.id} {...e} />
			))}
		</div>
	);
};

export default Column;
