import { useEffect, useState } from 'react';
import classNames from 'classnames';

import Link from './Link';

import { eol } from '../data/eol';
import type { TEOL } from '../data/eol';

interface Props {
	color?: string;
}

const EOL = ({ color }: Props) => {
	const [message, setMessage] = useState<TEOL>({
		text: '',
		href: '',
		link: '',
	});

	useEffect(() => {
		setMessage(eol[Math.floor(Math.random() * eol.length)]);
	}, []);

	return (
		<section className='section border-top-ui-2 py-5r'>
			<div className='container flex-c text-c'>
				<h2 className='fw-700 fs-8xl tablet-fs-7xl landscape-fs-6xl'>
					End of Line.
				</h2>

				<p className='mt-2r fs-xl'>
					{message.text}{' '}
					<Link href={message.href} mods={color} newTab>
						{message.link}
					</Link>
				</p>

				<button
					onClick={() =>
						window.scrollTo({
							top: 0,
							behavior: 'smooth',
						})
					}
					className={classNames(
						'j-button mt-2r cursor-pointer',
						color
					)}
				>
					Back to top
				</button>
			</div>
		</section>
	);
};

export default EOL;
