import { ChildrenOnly } from "../../types/utilities";

const Caption = ({ children }: ChildrenOnly) => (
	<p className="fs-lg text-dynamic-06 fw-500 ls-md">{children}</p>
);

export default Caption;