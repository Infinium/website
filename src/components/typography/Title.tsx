import classNames from "classnames";

interface TitleProps {
	children?: any,
	mods?: string
}

const Title = ({ children, mods = '' }: TitleProps) => (
	<h1 className={classNames('title', mods)}>{children}</h1>
);

export default Title;