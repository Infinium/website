import classNames from 'classnames';

import type { ComponentProps } from './types';

const Section = ({ id, children, mods = '' }: ComponentProps) => (
	<section id={id} className={classNames('section', mods)}>
		{children}
	</section>
);

export default Section;