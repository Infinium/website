import Image from 'next/image';

import View from './View';
import Caption from '../typography/Caption';
import Title from '../typography/Title';
import Link from '../Link';

interface Props {
	viewMods?: {
		section?: string;
		container?: string;
	};
	caption: string;
	title: string;
	content: React.ReactNode;
	link?: {
		label: string;
		href: string;
		newTab?: boolean;
	};
	linkArea?: React.ReactNode;
	image: {
		src: string;
		alt: string;
		width: number;
		height: number;
	};
}

const SplitSection = ({
	viewMods = undefined,
	caption,
	title,
	content,
	link = { href: '', label: '', newTab: false },
	linkArea = undefined,
	image,
}: Props) => (
	<View mods={viewMods}>
		<div className='w-100p grid grid-2 landscape-grid-1'>
			<div className='grid-block'>
				<Caption>{caption}</Caption>
				<Title>{title}</Title>

				<div className='mw-44r flex'>
					<p className='fs-2xl lh-1-7'>{content}</p>
					{linkArea ? (
						linkArea
					) : (
						<Link
							href={link.href}
							mods='button'
							isJLink={false}
							newTab={link.newTab}
						>
							{link.label}
						</Link>
					)}
				</div>
			</div>
			<div className='grid-block align-e landscape-align-c landscape-pt-3r'>
				<Image
					src={image.src}
					alt={image.alt}
					width={image.width}
					height={image.height}
				/>
			</div>
		</div>
	</View>
);

export default SplitSection;
