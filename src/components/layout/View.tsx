import Section from './Section';
import Container from './Container';

import type { ViewProps } from './types';

const View = ({ id = '', children, mods }: ViewProps) => (
	<Section id={id} mods={mods?.section}>
		<Container mods={mods?.container}>
			{children}
		</Container>
	</Section>
);

export default View;