import classNames from 'classnames';

import type { ComponentProps } from './types';

const Container = ({ children, mods = '' }: ComponentProps) => (
	<div className={classNames('container', mods)}>
		{children}
	</div>
);

export default Container;