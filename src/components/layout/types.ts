interface ComponentProps {
	id?: string,
	children: any,
	mods?: string
}

interface ViewProps {
	id?: string,
	children: any,
	mods?: {
		section?: string,
		container?: string
	}
}

export type { ComponentProps, ViewProps };