import { useTheme } from 'next-themes';

import Navigation from '../Navigation';
import Footer from '../Footer';
import Keyboard from '../Keyboard';

const Layout = ({ children }: { children: React.ReactNode }) => {
	const { theme, setTheme } = useTheme();

	return (
		<>
			<Navigation />

			{children}

			<Footer />

			<Keyboard
				keys={['t']}
				callback={(key, ev) =>
					setTheme(theme === 'dark' ? 'light' : 'dark')
				}
				handleFocusableElements
			/>
		</>
	);
};

export default Layout;
