import { motion } from 'framer-motion';
import classNames from 'classnames';

interface ButtonProps {
	children: React.ReactNode,
	mods?: string
}

const Button = ({ children, mods = '', ...props }: ButtonProps) => (
	<motion.button
		initial={{
			opacity: 1,
			y: 0
		}}
		whileHover={{
			opacity: 0.9,
			y: -2
		}}
		transition={{
			type: 'spring',
			duration: 0.8,
			bounce: 0.6
		}}
		className={classNames('button', mods)}
		{...props}
	>
		{children}
	</motion.button>
);

export default Button;