import Link from 'next/link';
import classNames from 'classnames';

import type { ArticleType } from '../lib/types';

import { colorMap } from '../lib/colors';

interface Props {
	title?: string;
	description?: string;
	image?: string;
	date?: string;
	slug?: string;
	type?: ArticleType;
}

const ArticleCard = ({
	title,
	description,
	image,
	date,
	slug,
	type,
}: Props) => {
	return (
		<Link href={`/article/${slug}`}>
			<a className='j-card clickable image-zoom flex-c'>
				<div className='j-card-image flex-c overflow-hidden'>
					<div
						className='j-card-image'
						style={{
							background: `url(${image})`,
							backgroundRepeat: 'no-repeat',
							backgroundSize: 'cover',
						}}
					></div>
				</div>
				<div className='j-card-content w-100p flex justify-sb flex-column flex-2'>
					<div>
						<div className='mb-0-75r'>
							<p
								className={classNames(
									'text-upper fw-600 fs-sm ls-md',
									`text-${colorMap[type || 'programming']}`
								)}
							>
								{type}
							</p>
						</div>
						<h4>{title}</h4>
						<p>{description}</p>
					</div>
					<div className='mt-1r'>
						<p className='fs-sm text-dynamic-07'>{date}</p>
					</div>
				</div>
			</a>
		</Link>
	);
};

export default ArticleCard;
