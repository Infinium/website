import Image from 'next/image';

import Link from './Link';

import type { ChildrenOnly } from '../types/utilities';

const Text = ({ children }: ChildrenOnly) => (
	<span className='text-upper ls-3xl ml-1r text-dynamic portrait-hide landscape-fs-sm'>
		{children}
	</span>
);

const Logo = () => (
	<Link
		href='/'
		mods='flex flex-row align-c justify-c logo-container'
		isJLink={false}
	>
		<Image
			src='/images/brand/logo-black.png'
			height={28}
			width={28}
			className='logo'
		/>
		<Text>Infinium</Text>
	</Link>
);

export default Logo;
