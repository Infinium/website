// Code slightly modified from:
// https://github.com/vercel/next.js/tree/canary/examples/blog-starter

import fs from 'fs';
import { join } from 'path';
import matter from 'gray-matter';

import type { Article } from './types';

const articlesDir = join(process.cwd(), '_articles');

const getSlugs = () => {
	return fs.readdirSync(articlesDir);
};

const getArticleBySlug = (slug: string, fields: any = []) => {
	const realSlug = slug.replace(/\.md$/, '');
	const fullPath = join(articlesDir, `${realSlug}.md`);
	const fileContents = fs.readFileSync(fullPath, 'utf8');
	const { data, content } = matter(fileContents);

	const items: any = {};

	fields.forEach((field: any) => {
		if (field === 'slug') {
			items[field] = realSlug;
		}
		if (field === 'content') {
			items[field] = content;
		}

		if (typeof data[field] !== 'undefined') {
			items[field] = data[field];
		}
	});

	return items;
};

const getAllArticles = (fields: any = []) => {
	const slugs = getSlugs();
	const articles = slugs
		.map((slug: string) => getArticleBySlug(slug, fields))
		.sort((a1: Article, a2: Article) => (a1.date > a2.date ? -1 : 1));
	return articles;
};

export { getSlugs, getAllArticles, getArticleBySlug };
