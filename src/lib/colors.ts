const colorMap = {
	'knowledge': 'magenta-50',
	'article': 'green-40',
	'how to': 'orange-60',
	'programming': 'cyan-40',
	'book review': 'orange-60',
	'opinion': 'magenta-50',
	'experience': 'teal-40',
	'philosophy': 'teal-40',
	'psychology': 'blue-60'
};

export {
	colorMap
}