type ArticleType = 
	'knowledge' |
	'article' |
	'how to' |
	'programming' |
	'book review' |
	'opinion' |
	'experience' |
	'philosophy' |
	'psychology';

interface Article {
	title?: string,
	description?: string,
	image: string,
	image_description?: string,
	date: string,
	slug?: string,
	content: string,
	type: ArticleType,
	author: string
}

export type { Article, ArticleType };
