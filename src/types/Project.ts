type Language = 
	'SCSS' |
	'JavaScript' |
	'TypeScript' |
	'Python' |
	'C' |
	'Rust' |
	'Go';

interface Project {
	id: string,
	name: string,
	description: string,
	repository: string,
	language: Language
}

export type { Project }