# Infinium website

View the [live website.](https://infinium.earth)

This is our business website. It's a NextJS site using TypeScript.

It's licensed under the MIT license.
