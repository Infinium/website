---
title: 'The 48 Laws of Power'
description: "A favorite among prison inmates, celebrities, and business executives alike, Robert Greene's 1998 bestseller has made quite the impression. Here's what I thought."
image: '/images/articles/48-laws/persian-c.jpeg'
image_description: 'Scythian messengers meet the Persian King Darius I by Franciszek Smuglewicz.'
date: 'July 25, 2021'
type: 'book review'
author: 'Tristan Brewster'
---

History speaks for itself.

Robert Greene uses no shortage of historical precedents to illustrate his discoveries. With every turn of the page, the reader is greeted with both abstract anecdotes in the form of margin-notes and a full page of power-related narratives.

The combination of notional tales, chronicled occurrences, abstract representations, established authorities, and possible realistic alternatives makes _The 48 Laws of Power_ an informative, compelling, and entertaining read. In short, I highly recommend it.

---

It seems to me the laws were taken, in part, either from famous historical examples or from quotes from various well-known philosophers, members of state, lawyers, and so on. The content of each chapter is mostly, as you'd expect, historical anecdotes describing either the transgressions or observances of the law in question. Each chapter ends with, to my delight, three reductive sections: **Image,** **Authority,** and **Reversal**

### The Image

Abstract in nature, the "Image" section of the chapter provides an ambiguous, often implicitly descriptive (oxymoronic, I know) summary of the law. For instance, in Law 1, "Never outshine the Master," the image explains:

> Image: The Stars in the Sky. There can be only one sun at a time. Never obscure the sunlight, or rival the sun's brilliance; rather, fade into the sky and find ways to heighten the master star's intensity.

I always look forward to the Image sections. It brilliantly describes the content of the chapter as a whole in such a way that you can easily remember the important aspects. I initially thought it'd be of more use to have a shorter, more abstracted explanation at the _beginning_ of the chapter, but I've since changed my mind. At the end, you will likely have forgotten some of the smaller details of the Law; having an abstracted summary at the _end_, Greene makes it much easier for you to grasp the content later down the line.

Not to mention, I find it much easier to recall some anecdotal, abstracted version than a highly detailed one.

### The Authority

Frequented by famous authors, philosophers, men of state, and so on, the Authority section provides external, non-Greene opinion on the Law you've just read. It often uses the exact words of the title of the Law itself.

The Authority, therefore, is even more historical proof that what you are learning is not only useful but globally applicable.

---

Without my own verification, I've noticed [Baltasar Gracián](https://en.wikipedia.org/wiki/Baltasar_Graci%C3%A1n) has had the most "Authority" throughout the book. Although not as well known as [Machiavelli](https://en.wikipedia.org/wiki/Niccol%C3%B2_Machiavelli) (who has also had his fair share of Authority throughout), Gracián has had his works praised by the likes of both [Arthur Schopenhauer](https://en.wikipedia.org/wiki/Arthur_Schopenhauer) and [Friedrich Nietzsche](https://en.wikipedia.org/wiki/Friedrich_Nietzsche).

To stay within context, the Authority for Law 1, by Gracián, is:

> Authority: Avoid outshining the master. All superiority is odious, but the superiority of a subject over his prince is not only stupid, it is fatal. This is a lesson that the stars in the sky teach us—they may be related to the sun, and just as brilliant, but they never appear in her company.

### The Reversal

Seen to me as the "realistic," (in a sense) portion of the chapter, the Reversal section is provided in the case the Law doesn't necessarily work within the context of your circumstances. Life is rarely so black and white. There are quite often cases where, in this context, outshining the master would do you a bit of good.

As such, _most_ laws have their respective reversals. In the case of Law 1, the relevant Reversal is:

> You cannot worry about upsetting every person you come across, but you must be selectively cruel. If your superior is a falling star, there is nothing to fear from outshining him. Do not be merciful—your master had no such scruples in his own cold-blooded climb to the top. Gauge his strength. If he is weak, discreetly hasten his downfall: Outdo, outcharm, outsmart him at key moments. If he is _very_ weak and ready to fall, let nature take its course. Do not risk outshining a feeble superior—it might appear cruel or spiteful. But if your master is firm in his position, yet you know yourself to be the more capable, bide your time and be patient. It is the natural course of things that power eventually fades and weakens. Your master will fall someday, and if you play it right, you will outlive and someday outshine him.

Some laws, like Law 35, "Master the art of timing," have no such such reversals. By not behaving properly, forgetting the Law, you're making a grave mistake. I always read twice the Laws that have no reversal; these are of upmost importance.

### What's the goal here?

Reading a book like this, one that advertises superiority, in public raises quite a few eyebrows. I'm sure many wonder, "uh-huh, he wants to be 'powerful,' right? How quaint."

My goal here is simple: knowledge. Intuitively, many people use some of these laws every now and then. It may be obvious that making yourself look better than your boss is a sure way to get yourself reprimanded (or worse). I don't want to manipulate people, I don't want to feel powerful; I simply want to understand how people naturally behave, and fit nicely within those confines.

In fact, my desire to fit within the confines of human nature is, more or less, a Law in and of itself: Law 45, "Preach the need for change, but never reform too much at once." Moreover, Law 38 is even more applicable: "Think as you like but behave like others."

### The Laws

1. Never outshine the master
2. Never put too much trust in friends, learn how to use enemies
3. Conceal your intentions
4. Always say less than necessary
5. So much depends on reputation—guard it with your life
6. Court attention at all costs
7. Get others to do the work for you, but always take the credit
8. Make other people come to you—use bait if necessary
9. Win through your actions, never through argument
10. Infection: Avoid the unhappy and unlucky
11. Learn to keep people dependent on you
12. Use selective honesty and generosity to disarm your victim
13. When asking for help, appeal to people's self-interest, never to their mercy or gratitude
14. Pose as a friend, work as a spy
15. Crush your enemy totally
16. Use absence to increase respect and honor
17. Keep others in suspended terror: cultivate an air of unpredictability
18. Do not build fortresses to protect yourself—isolation is dangerous
19. Know who you're dealing with—do not offend the wrong person
20. Do not commit to anyone
21. Play a sucker to catch a sucker—seem dumber than your mark
22. Use the surrender tactic: transform weakness into power
23. Concentrate your forces
24. Play the perfect courtier
25. Re-create yourself
26. Keep your hands clean
27. Play on people's need to believe to create a cult like following
28. Enter action with boldness
29. Play all the way to the end
30. Make your accomplishments seem effortless
31. Control the options: get others to play with the cards you deal
32. Play to people's fantasies
33. Discover each man's thumbscrew
34. Be royal in your own fashion: act like a King to be treated like one
35. Master the art of timing
36. Disdain things you cannot have: ignoring them is the best revenge
37. Create compelling spectacles
38. Think as you like but behave like others
39. Stir up waters to catch fish
40. Despise the free lunch
41. Avoid stepping into a great man's shoes
42. Strike the Shepard and the sheep will scatter
43. Work on the hearts and minds of others
44. Disarm and infuriate with the mirror effect
45. Preach the need for change, but never reform too much at once
46. Never appear too perfect
47. Do not go past the mark you aimed for; in victory, learn when to stop
48. Assume formlessness

### My opinion

I love this book. I'm quite fond of history as it is, but this book offers precisely what I'm looking for: people's mistakes and successes. Regardless of my choice to follow some of the rather immoral laws, I can still learn from each and every one of the anecdotes described.

If given the chance, I think you'll enjoy the book. Nearly 1.2 million people have bought it, and I can imagine _most_ have found something useful.