---
title: 'Method of Loci: Memory through Visualization'
description: "Swinging pink elephants. Archimedes dancing with Count Basie. Learn the effective and somewhat eccentric approach to memorization that'll stick with you for life."
image: '/images/articles/method-of-loci/athens-c.jpeg'
image_description: 'Athens, Greece'
date: 'July 23, 2021'
type: 'knowledge'
author: 'Tristan Brewster'
---

The story begins with my introduction to the phenomenon known as synesthesia. Subjects with this highly intriguing condition report feeling multiple connected mental sensations from a single strand of sensational input. In other words, they may sometimes associate a color with a sound or a smell with a touch. This concept of connected mental sensations ultimately led me to perhaps the most interesting (and useful) discovery I've made in the last few years.

## Mental relationships

Memory is not some static, one-way system of fetching values. [^1] Evident from subjects with synesthesia, it uses a system of association; one thing _reminds_ you of another. If memories were only accessed by a static data point, we wouldn't be of much use, would we? Memory, like a network, is a series of _interconnected_ points. A ladder may remind you of falling; falling may remind you of the anecdote Einstein used to imagine the physical effects of Relativity. [^2]

These mental relationships are foundational to our efficacy as a species. We've even went so far as to build social relationships around this central idea of a network.

It is the very nature of these mental relationships, this biological networking, that makes the Method of Loci possible.

## The Method

My introduction to the Method was from a combination of learning about synesthesia and watching the BBC network show _Sherlock_. Although rarely displayed, the title character uses what he calls the "Mind Palace Technique" to store and retrieve information.

Intrigued, I later stumbled upon an amazing book by Joshua Foer called _Moonwalking with Einstein_. This article will attempt to summarize the useful portions of the book; namely, how to memorize shit.

---

Before I explain the method itself, I first need to explain our natural capacity for environmental awareness.

Without thinking too much about it, I bet you can recall, more or less, the layout of your favorite local restaurant.

Our senses are used to gauge the environment we exist in. Thousands of years ago, our existence depended on our ability to determine our placement in the environment and the placement of valuable resources therein. Frankly, if you didn't remember where the water was, you were dead.

Because of this, we have come to have a talent for situational and environmental awareness. Try it. Close your eyes and imagine yourself walking from your front door to your room. Effortless, right? "Why are you even asking me something so elementary?" That's precisely the point.

---

From the book previously mentioned, Foer perfectly describes the inception of the Method of Loci:

> There were no other survivors.

> Family members arriving at the scene of the fifth-century-B.C. banquet hall catastrophe pawed at the debris for signs of their loved ones—rings, sandals, anything that would allow them to identify their kin for proper burial.

> Minutes earlier, the Greek poet Simonides of Ceos had stood to deliver an ode in celebration of Scopas, a Thessalian nobleman. As Simonides sat down, a messenger tapped him on the shoulder. Two young men on horseback were waiting outside, anxious to tell him something. He stood up again and walked out the door. At the very moment he crossed the threshold, the roof of the banquet hall collapsed in a thundering plume of marble shards and dust.

> He stood now before a landscape of rubble and entombed bodies. The air, which had been filled with boisterous laughter moments before, was smoky and silent. Teams of rescuers set to work frantically digging through the collapsed building. The corpses they pulled out of the wreckage were mangled beyond recognition. No one could even say for sure who had been inside. One tragedy compounded another.

> Then something remarkable happened that would change forever how people thought about their memories. Simonides sealed his senses to the chaos around him and reversed time in his mind. The piles of marble returned to pillars and the scattered frieze fragments reassembled in the air above. The stoneware scattered in the debris re-formed into bowls. The splinters of wood poking above the ruins once again became a table. Simonides caught a glimpse of each of the banquet guests at his seat, carrying on oblivious to the impending catastrophe. He saw Scopas laughing at the head of the table, a fellow poet sitting across from him sponging up the remnants of his meal with a piece of bread, a nobleman smirking. He turned to the window and saw the messengers approaching, as if with some important news.

> Simonides opened his eyes. He took each of the hysterical relatives by the hand and, carefully stepping over the debris, guided them, one by one, to the spots in the rubble where their loved ones had been sitting.

> At that moment, according to legend, the art of memory was born.

While I won't be explaining photographic memory (perhaps I'll save that for another article), I will be explaining how to use your natural talent for environmental awareness to your advantage.

### Using the Method

When I say I've found this Method to be useful, I'm speaking the truth. It was about 4 years ago that I created my first Mind Palace. Since then, I've rarely "revisited" it. Without much effort at all, I can perfectly recall every single detail as if I had just made it—all I must do is walk through it again.

We'll first begin by creating a small Palace with only a few items.

Start by thinking of a physical place you know well. Your current house will do fine.

Now begin walking through it in your mind. The precise details aren't too important, but you've _got_ to have the layout down.

We first need to create a specific path—a certain sequence of turns that you will _always_ follow every time you revisit your Mind Palace. This is an important aspect. By changing the path you take, you're at risk of not seeing some of the things you place. I'll come back to this in a moment.

Walk this path a few times. For me, I started at the front door, walked into the kitchen, into the guest bedroom, and finally to my room. For good measure, why not walk the path a few more times? Your mind may naturally want to jump to locations of interest; try your best to slowly walk the path as if you were physically walking.

### Recalling a shopping list

We now need something to remember. To begin, we should start with a handful of ordinary items. Feel free to use a different list if the one below doesn't suit you; it should be about the same length and with items of a similar simplicity.

- Sour cream
- Hot tub
- Socks
- White wine
- Television
- Fog machine

The characteristic aspect of the Method is to use vivacious and abnormal representations of the things you want to recall. In essence, you need to get **imaginative**.

"Sour cream" may seem dull and ordinary; to be able to remember it, though, you need to make it into something lively and animated.

Off the top of my head, I'd imagine a young baby smothering it all over his face, laughing loudly as he does it. Maybe he's jumping up and down on a sour cream trampoline!

Now that's memorable!

The "sour cream baby" I spoke of was merely an example. Throughout this article, you'll want to create and imagine your own items; imaginations of your own are far more powerful and applicable to you.

---

Recall the path you've formulated. We've created this path to be able to place "items" along it, such that when you walk through it again, you'll "see" the things you wanted to remember.

Navigate back to the beginning of your path. Now, right in the walkway, simply place that vivacious and lively sour cream item you've imagined.

Spend a few moments really trying to visualize it. What does it sound like? What does it feel like to touch it? How heavy is it? All of these data points are useful for remembrance.

As you do this more often, you'll find you need to spend less and less time on each object for it to truly solidify.

---

Let's move on to object two. Go back to the beginning of your path, consciously take note of the sour cream, and move a little past it.

Find a nice, close-ish place to put your next item: the hot tub.

Let's make this "hot tub" more than just a hot tub. Let's fill it with fully clothed, winter-ready people. 

They're clothed in down jackets, snow pants, and beanies. In fact, they're Norwegian! 

They're chanting, holding wine glasses (connections with other items on your "list" is also useful), and sweating horribly.

How loud are they? How much water has spilled onto your nice home floor?

Now go back to the beginning. You read that right. After placing any object, go back to the very beginning of your Mind Palace and walk through it again. This repetition will help.

---

Since it'd be a little arduous to go through each item in this article, I'll leave that honor to you.

The list now is

- Socks
- White wine
- Television
- Fog machine

Go through every item, imagine it in the most vivacious, creative way you can, place it on the path, go back to the beginning, and repeat the sequence again.

Over the next few days, you'll want to occasionally revisit your Mind Palace, just to get used to visiting it.

### It's just the beginning

Being able to remember a shopping list can come in handy quite often. A list of nouns is rather easy: simply visualize the object itself. What about numbers? What about abstract thoughts? What about every single word from your favorite book (not kidding)?

The thing I admire the most about the Mind Palace technique is the imagination. You _must_ be creative to be successful in this art. The more you do it, the easier these _internal visualizations_ will come. Although I only surmise, I'd wager the consistent imaginative aspect of this Method is useful in many other areas—creativity in general, for one.

#### Remembering numbers

To recall a list of numbers, we can use the [Mnemonic major system](https://en.wikipedia.org/wiki/Mnemonic_major_system). As Wikipedia explains, "The system works by converting numbers into consonants, then into words by adding vowels."

The system is rather simple: take your list of numbers, convert each into some letter, and then convert that into some noun-like thing that you can visualize.

I'll briefly outline the system below; if you want a more thorough explanation, view the Wikipedia article about it.

- **0**: s, soft c, z, x (in xylophone)
- **1**: t, d , th (both in thing and this)
- **2**: n
- **3**: m
- **4**: r, l (as sounded in colonel)
- **5**: i
- **6**: (verbatim from Wikipedia) ch (in cheese and chef), j, soft g, sh, c (as sounded in cello and special), cz (as sounded in Czech), s (as sounded in tissue and vision), sc (as sounded in fascist), sch (as sounded in schwa and eschew), t (as sounded in ration and equation), tsch (in putsch), z (in seizure)
- **7**: k, hard c, q, hard g, ch (as sounded in loch)
- **8**: f, ph (in phone), v, gh (as sounded in laugh)
- **9**: p, b

It may look intimidating, but it's rather simple.

Let's start with a simple number: 438

For 4, we can choose either "r" or "l." I'll go with "r" for the time being.

For 3, we are limited to "m."

For 8, we have many options: f, ph, v, and gh

With the number 438, I'll choose the letters "R-M-V" for our final "word." That looks quite a lot like the word "remove." Great!

The only hurdle now is to translate this "remove" into some visualization we can remember.

I imagine "remove" as a tall, sturdy brick wall with contractors violently ripping out bricks with sledgehammers and pickaxes. When you see that, you may recall the valuable "R-M-V" letters, which translate to "4-3-8."

After some time has passed, the visualization you pair with the numbers may become less connected. In other words, you may forget the relevancy. Because of this, I like to add the numbers themselves, unaltered, above the scene to help me remember. In this case, I'd see the numbers "438" in white text hovering above the brick wall. That way, I can just look up to see what I'm supposed to remember. Obviously, this may also become faded, but by adding a separate entity to the same idea, you'll strengthen the mental connection to the numbers.

### You'll need more locations...

Before I even attempt to describe how to memorize abstract objects, you'll need a lot more experience.

This article is not a "become a memory champion in 4 easy steps" kind of article. It's meant to be bookmarked and returned to on many occasions, as you progress through your technique.

Near the end, I'll cover abstract objects; for now, though, let's focus on the locations themselves.

Eventually, as you create more Mind Palace's, you will run out of locations. Sure, you _could_ use the gym down the street as a location, but that doesn't always _feel_ right.

There are two primary ways to "create" or "get" more locations: 1. Imagine entirely new places on your own; or 2. Change the scale of your perspective and use smaller locations.

When I was first introduced to the Method, I initially read that it's common to use smaller locations, like your car, as a Palace. Although it may feel somewhat strange, this offers a plethora of other locations you'd never considered.

If you got tiny enough, you could even use your keyboard as a Palace if you wanted.

The location and the scale therein is entirely up to you. I prefer to imagine new places—it helps to strengthen the mental connection to the whole visualization process.

Although it may seem simple, this step is quite important. What if you really need to remember a list but you can't think of a place to put it? It's always nice to have excess locations ready for action. Not to mention, it's fun to think about locations you could use and revisit later on.

### Connecting the senses

You may ask, "how can I make these memories _even more_ memorable?"

I'll tell you: by adding more attributes.

In the beginning of this article I explained my interest in synesthesia. Now is the time to truly use it to our advantage.

As explained, synesthesia is responsible for "connecting" multiple sensations; as the word "sensations" suggests, it's most commonly associated with our physical senses. (We have more than five, by the way [^3])

I've occasionally hinted to this aspect throughout my description of the Method. I asked, "what does it sound like?" and, "how much does it weigh?"

What I'm trying to do is get your mind to think about the other aspects of the object. By considering these finer details, you're adding more attributes to the object itself; by adding more attributes, you're making it easier to retrieve the memory later down the line.

While it's still incredibly useful to use sight alone, it's even more powerful if you can _hear_ or _feel_ the object as you walk along your path.

For instance, the last item, the fog machine, could be actively being used to produce fog; when you walk through it, you not only _see_ the fog, you likewise _feel_ the slightly cold smoke flowing over your skin.

There's a balance to be made here: either make the object incredibly vivacious and animated or connect the object to multiple senses (both is preferable).

### Abstract objects

Physical objects and numbers can only get you so far. After a certain point, you'll find the need to store and recall abstract objects, thoughts, and so on. These aren't so easily translated into some visualization that you can absorb with physical senses.

Take the book, _The World as Will and Representation_ by Arthur Schopenhauer. How do you remember his basic philosophy behind the book as a whole?

For things of this nature, things not so easily translated into reality, I like to use Free association. [^4]

With a few details excluded, this is the process of naturally allowing "what comes first" to leave your mind, by speaking or writing, without censorship or judgement. In essence, it's similar to sitting down with a piece of a paper and just writing whatever comes to mind without thinking about it.

Abstract entities, like the philosophy of Schopenhauer will almost always remind you of some other thing—something more easily translated into reality.

Let's go through a simple example.

With **many** details left out, we can talk about the "representation" part of Schopenhauer's view. As Wikipedia explains it, "Schopenhauer argues that the world humans experience around them—the world of objects in space and time and related in causal ways—exists solely as ‘representation’ dependent on a cognizing subject, not as a world that can be considered to exist in itself (i.e. independently of how it appears to the subject’s mind)."

Without thinking too much about it, this sort of reminds me of a mask of reality. Objects exist, sure, but not as _I_ perceive them. Because of this, my visual representation of this explanation may include a stage performer removing an infinite amount of masks—never revealing his or her true identity.

Once again, it's this imagination, this creation of representation, that is the _essence_ of the Method. When at a loss for visualizations, try Free associations—let it just "come to you."

Like I've said before, you'll become better at this with time and experience.

### Closing remarks

If you're serious about the Method, come back to this article every so often. The repetition and consistent explanations will help to solidify the important aspects of it.

I know this is easy to discard as some elementary, useless process; after all, it takes quite a lot of mental time and effort. I urge you, however, to give it a sincere try. Even though a measly 10-item shopping list may be effortless to remember in the first place, you'll eventually find great need to remember things of even greater cardinality.

The Method will surely help with that.

I hope you enjoyed the read!

[^1]: [Configural association theory: The role of the hippocampal formation in learning, memory, and amnesia](https://doi.org/10.3758/BF03337828)

[^2]: [Einstein's thought experiments](https://en.wikipedia.org/wiki/Einstein's_thought_experiments#Falling_painters_and_accelerating_elevators)

[^3]: [Humans have a lot more than five senses — here are 18](https://www.considerable.com/health/healthy-living/humans-five-senses/)

[^4]: [Free association (psychology)](https://en.wikipedia.org/wiki/Free_association_(psychology))